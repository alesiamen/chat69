import ui from './ui'
import store from './store'
import audio from '../static/audio.mp3'

let editedMessageId

window.onfocus = () => {
  document.title = 'Chat'
}

function notifyUser() {
  if (document.hidden) {
    new Audio(audio).play()
    document.title = 'New message - Chat.'
  }
}

function render(msg, user) {
  if (msg.type === 'normal') {
    ui.renderMessage(msg, msg.name === user.displayName, () => {
      ui.deleteMessage(msg.id)
      store.deleteMessage(msg.id)
    })
  }
  if (msg.type === 'system') {
    ui.renderSystem(msg)
  }
  ui.scrollToLastMessage()
  notifyUser()
}

export default {
  start(user) {
    store.setUserOnline(user)

    store.onUsersOnline(ui.renderUsersOnline)

    store.onMessage(m => render(m, user))

    store.onMessageModified(m => ui.updateMessage(m.id, m.text))

    store.onHistoryLoaded(() => store.userJoinChat(user))

    ui.onSubmit(text => {
      if (editedMessageId) {
        store.updateMessage(editedMessageId, text)
        editedMessageId = null
      } else {
        store.addMessage(user, text)
      }
    })

    ui.onMessageEdit(msg => {
      if (msg.name === user.displayName) {
        editedMessageId = msg.id
        store.getMessage(msg.id).then(msg => {
          ui.setInputValue(msg.text)
        })
      }
    })

    store.onTypers(ui.renderTypers)

    let timeoutId

    ui.onTyping(() => {
      clearTimeout(timeoutId)
      store.addTyper(user)
      timeoutId = setTimeout(() => store.deleteTyper(user), 1000)
    })

    window.onfocus = () => store.setUserOnline(user)
    window.onbeforeunload = () => store.setUserOffline(user)
  },
}
