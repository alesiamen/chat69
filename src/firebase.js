import firebase from '@firebase/app'
import '@firebase/firestore'
import '@firebase/auth'

if (!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: 'AIzaSyDxUYzaGZRl-ryhTn5nNT0mHctaqSLU8VU',
    authDomain: 'chata-8cee6.firebaseapp.com',
    databaseURL: 'https://chata-8cee6.firebaseio.com',
    projectId: 'chata-8cee6',
    storageBucket: 'chata-8cee6.appspot.com',
    messagingSenderId: '545922988059',
  })
}

export default firebase
