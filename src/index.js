import firebase from './firebase'
import './styles.css'
import chat from './chat'

firebase.auth().onAuthStateChanged(user => {
  if (user) {
    chat.start(user)
  } else {
    const provider = new firebase.auth.GoogleAuthProvider()
    firebase.auth().signInWithPopup(provider)
  }
})
