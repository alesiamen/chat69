import firebase from './firebase'

let db
let historyLoaded
let historyCallback
let newestMessage = 0

if (db === undefined) {
  db = firebase.firestore()
}

export default {
  getMessage(id) {
    return db
      .collection('messages')
      .doc(id)
      .get()
      .then(doc => doc.data())
  },

  addMessage(user, text) {
    db.collection('messages').add({
      type: 'normal',
      text: text,
      name: user.displayName,
      photoURL: user.photoURL,
      createdAt: Date.now(),
    })
  },

  deleteMessage(id) {
    db
      .collection('messages')
      .doc(id)
      .delete()
  },

  updateMessage(id, editedText) {
    db
      .collection('messages')
      .doc(id)
      .update({
        text: editedText,
      })
  },

  onMessage(cb) {
    const unsubscribe = db
      .collection('messages')
      .orderBy('createdAt', 'desc')
      .limit(20)
      .onSnapshot(snapshot => {
        const history = []

        snapshot.docChanges.forEach(change => {
          if (change.type === 'added') {
            const msg = change.doc.data()
            msg.id = change.doc.id
            if (!historyLoaded) {
              newestMessage = msg.createdAt
              return history.push(msg)
            }

            if (msg.createdAt > newestMessage) {
              newestMessage = msg.createdAt
              cb(msg)
            }
          }
        })

        if (!historyLoaded) {
          historyLoaded = true
          history.reverse().forEach(cb)
          this.onHistoryLoaded()
        }
      })

    if (module.hot) {
      module.hot.dispose(unsubscribe)
    }
  },

  onHistoryLoaded(cb) {
    if (!historyLoaded) {
      historyCallback = cb
    } else {
      historyCallback()
    }
  },

  onMessageModified(cb) {
    const unsubscribe = db.collection('messages').onSnapshot(snapshot => {
      snapshot.docChanges.forEach(change => {
        if (change.type === 'modified') {
          const msg = change.doc.data()
          msg.id = change.doc.id
          cb(msg)
        }
      })
    })

    if (module.hot) {
      module.hot.dispose(unsubscribe)
    }
  },

  addTyper(user) {
    db
      .collection('typers')
      .doc(user.displayName)
      .set({
        name: user.displayName,
      })
  },

  deleteTyper(user) {
    db
      .collection('typers')
      .doc(user.displayName)
      .delete()
  },

  onTypers(cb) {
    const unsubscribe = db.collection('typers').onSnapshot(snapshot => {
      const typers = []
      snapshot.forEach(doc => typers.push(doc.data().name))
      cb(typers)
    })

    if (module.hot) {
      module.hot.dispose(unsubscribe)
    }
  },

  userJoinChat(user) {
    db.collection('messages').add({
      type: 'system',
      text: 'User ' + user.displayName + ' is in chat now',
      createdAt: Date.now(),
    })
  },

  setUserOnline(user) {
    db
      .collection('online')
      .doc(user.displayName)
      .set({
        name: user.displayName,
      })
  },

  setUserOffline(user) {
    db
      .collection('online')
      .doc(user.displayName)
      .delete()
  },

  onUsersOnline(cb) {
    const unsubscribe = db.collection('online').onSnapshot(snapshot => {
      const users = []
      snapshot.forEach(doc => users.push(doc.data().name))
      cb(users)
    })

    if (module.hot) {
      module.hot.dispose(unsubscribe)
    }
  },
}
