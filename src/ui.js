import store from './store'

import EmojiPicker from 'vanilla-emoji-picker'

import basicContext from 'basiccontext'
import 'basiccontext/dist/basicContext.min.css'
import 'basiccontext/dist/themes/dark.min.css'
import 'basiccontext/dist/addons/popin.min.css'

let editCb

document.addEventListener('DOMContentLoaded', () => new EmojiPicker())

export default {
  onSubmit(cb) {
    form.onsubmit = e => {
      e.preventDefault()
      messageInput.value && cb(messageInput.value)
      messageInput.value = ''
    }
  },

  onMessageEdit(cb) {
    editCb = cb
  },

  onTyping(cb) {
    messageInput.oninput = e => cb(messageInput.value)
  },

  scrollToLastMessage() {
    msgBox.lastChild.scrollIntoView()
  },

  setInputValue(value) {
    messageInput.value = value
  },

  renderMessage(msg, toLeft, onDelete) {
    const node = document.createElement('div')
    node.title = new Date(msg.createdAt).toLocaleString()
    node.setAttribute('data-id', msg.id)
    var text = `${msg.text}<div class="usr-name">${msg.name}</div>`

    if (!toLeft) {
      node.className = 'msg-left'
      node.innerHTML = `<img class="usr-photo" src = "${msg.photoURL}">` + text
    } else {
      node.className = 'msg-right'
      node.innerHTML = text
    }

    node.addEventListener('contextmenu', function(e) {
      let items = [
        { title: 'Edit', icon: 'ion-plus-round', fn: () => editCb(msg) },
        { title: 'Delete', icon: 'ion-person', fn: () => onDelete() },
      ]

      basicContext.show(items, e)
    })
    msgBox.appendChild(node)
  },

  deleteMessage(id) {
    const node = document.querySelector(`[data-id="${id}"]`)
    if (node) {
      node.remove()
    }
  },

  updateMessage(id, text) {
    const node = document.querySelector(`[data-id="${id}"]`)
    node.textContent = text
  },

  renderSystem(msg) {
    const messageNode = document.createElement('div')
    messageNode.className = 'msg-center'
    messageNode.innerHTML = `
     ${msg.text}
     <div class="msg-date">${new Date(msg.createdAt).toLocaleString()}</div>
  `
    msgBox.appendChild(messageNode)
  },

  renderTypers(typers) {
    if (typers.length) {
      typingInput.textContent =
        typers.join(', ') + (typers.length > 1 ? ' are' : ' is') + ' typing...'
    } else {
      typingInput.textContent = ''
    }
  },

  renderUsersOnline(users) {
    onlineUsers.textContent = 'In chat now: \r\n' + users.join(', ')
  },
}
